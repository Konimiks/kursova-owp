const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const mongodb = require('promised-mongo');

const url = 'mongodb://localhost:27017/course-work';
const db = mongodb(url);

function sendError(res, reason) {
    res.status(500).json({ error: String(reason) });
}

function sendResult(res, result) {
    res.json({ result });
}

router.use(bodyParser.urlencoded({ extended: true}));
router.use(bodyParser.json());

router.get('/createadmin', (req, res) =>{
    let registrationDate = Date();
    db.users.findOne({ login: "konimiks"})
        .then(user => {
            if(user){
                res.send('false');
            }
            else{
                db.users.insert({
                    login: "konimiks",
                    passwordHash: "abc404c4269afcf99e793d2caab1125f",
                    firstName: "Дмитро",
                    lastName: "Дяченко",
                    role : "admin",
                    avatar_id : null,
                    recipesNumber: 0,
                    registrationDate
                })
                    .then(() => res.send('ok'))
            }
        })
        .catch(err => res.status(500).end(err));
});

router.delete('/delete_recipe/:recipe_id', (req, res) => {
    db.recipes.remove({
        _id: mongodb.ObjectId(req.params.recipe_id)
    }, true)
        .then(x => sendResult(res, x))
        .catch(err => sendError(res, err));
});

router.delete('/delete_user/:user_id', (req, res) => {
    db.users.remove({
        _id: mongodb.ObjectId(req.params.user_id)
    }, true)
        .then(x => sendResult(res, x))
        .catch(err => sendError(res, err));
});

router.get('/users', (req, res) => {
    db.users.find()
        .then(users => sendResult(res, users))
        .catch(err => sendError(res, err));
});

router.get('/users/:user_id', (req, res) =>{
   db.users.findOne({ _id: req.params.user_id})
       .then(user => sendResult(res, user))
       .catch(err => sendError(res, err));
});

router.get('/recipes', (req, res) => {
    db.recipes.find()
        .then(recipes => sendResult(res, recipes))
        .catch(err => sendError(res, err));
});

router.get('/recipes/:recipe_id', (req, res) => {
    let id = req.params.recipe_id;
    db.recipes.findOne({_id: mongodb.ObjectId(id)})
        .then(recipe => sendResult(res, recipe))
        .catch(err => sendError(res, err));
});

router.post('/upload-recipe', (req, res) => {
    let author_id = req.user._id;
    let name = req.body.name;
    let description = req.body.desc;
    if(name && description){
        db.recipes.insert({
            name,
            description,
            author_id
        })
            .then(recipes => {
                db.users.findOne({ _id: mongodb.ObjectId(author_id)})
                    .then(user => {
                        let num = user.recipesNumber;
                        num++;
                        db.users.findAndModify({
                            query: {_id: mongodb.ObjectId(author_id)},
                            update: {$set: {recipesNumber: num}},
                            new: false
                        })
                            .then(() => sendResult(res, recipes))
                    })
            })
            .catch(err => sendError(res, err));
    }
    else {
        res.sendResult(res, "Not added");
    }
});

router.post('/register', (req, res) => {
        let login = req.body.login;
        let firstName = req.body.firstName;
        let lastName = req.body.lastName;
        let pass = req.body.inputPassword;
        let pass2 = req.body.confirmPassword;
        if (!login || !pass || pass !== pass2 || !firstName || firstName == "" ||
            !lastName || lastName == "" || pass.length < 6) {
            res.sendError(res, "Invalid input!");
        }
        db.users.findOne({ login: login})
            .then(x => {
                if (x) {
                    sendResult(res, "Логин занят");
                } else {
                    db.users.insert({
                        login,
                        passwordHash: hash(pass),
                        firstName,
                        lastName,
                        role : "user",
                        avatar_id : null,
                        recipes : null
                    })
                        .then((x) => sendResult(res, x))
                        .catch(err => res.sendError(res, "Invalid input!"));
                }
            })
            .catch(err => res.sendError(res, err));
});

module.exports = router;