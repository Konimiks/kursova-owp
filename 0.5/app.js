const express = require('express');
var path = require('path');
const bodyParser = require('body-parser');
const mongodb = require('promised-mongo');
const busboyBodyParser = require('busboy-body-parser');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const session = require('express-session');

//Routers:
const apiRouter = require('./routes/api');

const app = express();
const url = 'mongodb://localhost:27017/course-work';
const db = mongodb(url);

let sessionSecret = "%$^You never know?you never know!$%(#@_-=";
const salt = 'wtejbhoweuithi&%^&*&HI#OJBJN3f2h3jh _34tf234';

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(busboyBodyParser({ limit: '5mb' }));
app.use(cookieParser());
app.use(session({
  secret: sessionSecret,
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));

function sendError(res, reason) {
  res.status(500).json({ error: String(reason) });
}

function sendResult(res, result) {
  res.json({ result });
}


passport.serializeUser(function(user, done) {
  done(null, user._id);
});
passport.deserializeUser(function(id, done) {
  console.log("deserializeUser id: " + id);
  db.users.findOne({ _id: mongodb.ObjectId(id) })
      .then(user => {
        if(user) {
          done(null, user);
        } else {
          done("No user", null);
        }
      })
      .catch(err => done(err, null));
});
passport.use(new LocalStrategy((username, password, done) => {
  console.log("Local: " + username + " : " + password);
  db.users.findOne({
    login: username,
    passwordHash: hash(password)
  })
      .then(user => {
        console.log(user);
        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      })
      .catch(err => {
        console.log(err);
        done(err, null);
      });
}));

function hash(pass) {
  return crypto.createHash('md5').update(pass + salt).digest("hex");
}

//Routers:
app.use("/api", apiRouter);

//app.get('/', (req, res) => res.render('users_index', { user: req.user }));

app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});

//app.get('/users', (req, res)=> db.users.find().then(users => res.json(users)));


app.get('/login', (req, res) => {
  res.render("login", {msg : req.query.msg} );
});

app.post('/login',
    passport.authenticate('local', { failureRedirect: '/' }),
    (req, res) => res.redirect('/'));

app.post('/delete_recipe/:recipe_id', (req, res) => {
    db.recipes.findOne({_id: mongodb.ObjectId(req.params.recipe_id)})
        .then(recipe => {
            let author_login = recipe.author_login;
            db.users.findOne({ login: author_login})
                .then(user => {
                    let num = user.recipesNumber;
                    num--;
                    db.users.findAndModify({
                        query: {login: author_login},
                        update: {$set: {recipesNumber: num}},
                        new: false
                    })
                        .then(() => {
                            db.recipes.remove({
                                _id: mongodb.ObjectId(req.params.recipe_id)
                            }, true)
                                .then(() => {
                                    db.recipes.find()
                                        .then(recipes => res.render("recipes", {recipes: recipes, user: req.user}))
                                })
                                .catch(err => sendError(res, err));
                        })
                })
        })
        .catch(err => sendError(res, err));
});

app.post('/register',
    (req, res) => {
      let login = req.body.login;
      let firstName = req.body.firstName;
      let lastName = req.body.lastName;
      let pass = req.body.inputPassword;
      let pass2 = req.body.confirmPassword;
      console.log(login);
      if (!login || !pass || pass !== pass2 || !firstName || firstName == "" ||
              !lastName || lastName == "" || pass.length < 6) {
        sendError(res, "Invalid input!");
      }
      let registrationDate = Date();
      db.users.findOne({ login: login})
          .then(x => {
            if (x) {
              res.sendError(res, "login is busy");
            } else {
              db.users.insert({
                login,
                passwordHash: hash(pass),
                firstName,
                lastName,
                role : "user",
                avatar_id : null,
                recipesNumber: 0,
                registrationDate
              })
                  .then(() => res.redirect('/'))
                  .catch(err => res.status(500).end(err));
            }
          })
          .catch(err => res.status(500).end(err));
    });

app.get('/checklogin', (req, res) => {
    db.users.findOne({login : req.query.login})
        .then(book => {
            if(book)
                res.sendStatus(400);
            else res.sendStatus(200);
        })
        .catch(err => sendError(res, err));
});

app.get('/checkrecipename', (req, res) => {
    db.recipes.findOne({name : req.query.name})
        .then(recipe => {
            if(recipe)
                res.sendStatus(400);
            else res.sendStatus(200);
        })
        .catch(err => sendError(res, err));
});

/*
app.get('/', (req, res) =>{
    res.render("index", {msg : req.query.msg , user: req.user});
});*/


app.get("/", (req,res) =>{
    db.users.find().sort({
        recipesNumber: -1
    }).limit(4)
        .then(authors => {
            db.avatars.findOne({ _id: mongodb.ObjectId(authors[0].avatar_id)})
                .then(ava1 => {
                    db.avatars.findOne({_id: mongodb.ObjectId(authors[1].avatar_id)})
                        .then(ava2 => {
                            db.avatars.findOne({_id: mongodb.ObjectId(authors[2].avatar_id)})
                                .then(ava3 => {
                                    db.avatars.findOne({_id: mongodb.ObjectId(authors[3].avatar_id)})
                                        .then(ava4 => {
                                            res.render("index", {user: req.user, authors: authors,
                                            ava1, ava2, ava3, ava4});
                                        })
                                })
                        })
                })
        })
        .catch(err => sendError(res, "Error while loading page"));
});

app.get('/profile', (req,res) =>{
  if(req.user){
    db.avatars.findOne({ _id :  mongodb.ObjectId(req.user.avatar_id)})
        .then(x => {
          res.render('user_index', {user: req.user, avatar: x});
        })
        .catch(err => sendError(res, err));
  }
  else{
    sendError(res, "Not authorised");
  }
});

/*
app.get("/avatars", (req, res) => {
  db.avatars.find()
      .then(x => sendResult(res, x))
      .catch(err => sendError(res, err));
});

app.delete('/delete_av/:avid', (req, res) => {
  db.avatars.remove({
    _id: mongodb.ObjectId(req.params.avid)
  }, true)
      .then(x => sendResult(res, x))
      .catch(err => sendError(res, err));
});*/

app.get('/search', (req, res) => {
    if(req.query.search == "") {
        db.recipes.find()
            .then(recipes => {
                res.send(JSON.stringify(recipes))
            })
            .catch(err => sendError(res, err));
    }
    else {
        db.recipes.find({name: req.query.search})
            .then(recipes => {
                res.send(JSON.stringify(recipes))
            })
            .catch(err => sendError(res, err));
    }
});

app.get('/recipes', (req, res) => {
  db.recipes.find()
      .then(recipes => res.render("recipes", {recipes: recipes, user: req.user}))
      .catch(err => sendError(res, err));
});

app.get('/recipes/:recipe_id', (req, res) => {
  let id = req.params.recipe_id;
  db.recipes.findOne({_id: mongodb.ObjectId(id)})
      .then(recipe => res.render("recipe_index", {recipe: recipe}))
      .catch(err => sendError(res, err));
});

app.get("/addrecipe", (req, res) =>{
    res.render("addrecipe", {user: req.user});
});

//
//   UPLOADING RECIPE
//
app.post('/upload-recipe', (req, res) => {
    if(!req.body.name || !req.body.author_login){
        sendError(res, "Error, no name or no author")
    }
    else {
        let name = req.body.name;
        db.recipes.findOne({name : name})
            .then(recipe => {
                if(recipe){sendError(res, "Error, name is busy")}
                else{
                    let author_login = req.body.author_login;
                    db.users.findOne({login : author_login})
                        .then((author) => {
                            if(author){
                                let ingredient0 = {
                                    name: req.body.ingredient_name0,
                                    quantity: req.body.ingredient_count0,
                                    measurement: req.body.ingredient_measurement0
                                }
                                let ingredient1 = {
                                    name: req.body.ingredient_name1,
                                    quantity: req.body.ingredient_count1,
                                    measurement: req.body.ingredient_measurement1
                                }
                                let ingredient2 = {
                                    name: req.body.ingredient_name2,
                                    quantity: req.body.ingredient_count2,
                                    measurement: req.body.ingredient_measurement2
                                }
                                let ingredient3 = {
                                    name: req.body.ingredient_name3,
                                    quantity: req.body.ingredient_count3,
                                    measurement: req.body.ingredient_measurement3
                                }
                                let ingredients = { ingredient0, ingredient1, ingredient2, ingredient3 };
                                let instruction = req.body.instruction;
                                let portion_quantity = req.body.portion_count;
                                let cooking_time = {
                                    hours: req.body.cooking_time_hours,
                                    minutes: req.body.cooking_time_minutes
                                }
                                let category = req.body.category;
                                let kitchen = req.body.kitchen;
                                let date = Date();
                                let file = req.files.image;
                                if(!file) res.sendError(res, "Error while convertion image");
                                let image = file.data.toString('base64');
                                db.recipes.insert({
                                    name,
                                    author_login,
                                    ingredients,
                                    instruction,
                                    portion_quantity,
                                    cooking_time,
                                    category,
                                    kitchen,
                                    date,
                                    image
                                })
                                    .then(() => {
                                        db.users.findOne({ login: author_login})
                                            .then(user => {
                                                let num = user.recipesNumber;
                                                num++;
                                                db.users.findAndModify({
                                                    query: {login: author_login},
                                                    update: {$set: {recipesNumber: num}},
                                                    new: false
                                                })
                                                    .then(() => res.redirect('/recipes'))
                                            })
                                    })
                                    .catch(err => res.sendError(res, "Error while adding!"));
                            }
                            else sendError(res, "Error, author doesn`t exist");
                        })
                        .catch(err => sendError(res, err));
                }
            })
            .catch(err => sendError(res, err));
    }
});

app.post('/upload', (req, res) =>{
  let avaFile = req.files.avatar;
  if(avaFile) {
    let base64String = avaFile.data.toString('base64');
    db.avatars.insert({
      image: base64String
    })
        .then((x) => {
          db.users.findOne({_id: mongodb.ObjectId(req.user._id)})
              .then((user) => {
                if (user) {
                  db.avatars.remove({_id: user.avatar_id})
                      .then(() => {
                          db.users.findAndModify({
                            query: {_id: req.user._id},
                            update: {$set: {avatar_id: x._id}},
                            new: false
                          })
                              .then(() => res.redirect('/profile'))
                      })
                      .catch(err => sendError(res, err));
                }
              })
        })
        .catch(err => res.status(500).end(err));
  }
  else {
    res.redirect('/profile');
  }
});

app.listen(3100, () => console.log("Course-work listen at 3100"));


