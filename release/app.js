console.log("started");
const express = require('express');
var path = require('path');
const bodyParser = require('body-parser');
const mongodb = require('./promised-mongo1/promised-mongo-master/index');
const busboyBodyParser = require('busboy-body-parser');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const session = require('express-session');
const csrf = require('csurf');

const app = express();

//Routers:
const apiRouter = require('./routes/api');
app.use("/api", apiRouter);

const url ='mongodb://konimiks:dragon123123@ds159507.mlab.com:59507/course-work';
//const url = 'mongodb://localhost:27017/course-work';
const db = mongodb(url, {
    authMechanism: 'ScramSHA1'
});

const sessionSecret = "%$^You never know?you never know!$%(#@_-=";
const salt = 'wtejbhoweuithi&%^&*&HI#OJBJN3f2h3jh _34tf234';

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(busboyBodyParser({ limit: '5mb' }));
app.use(cookieParser());
app.use(session({
  secret: sessionSecret,
  resave: false,
  saveUninitialized: true
}));
var csrfProtection = csrf({ cookie: true });
app.use(passport.initialize());
app.use(passport.session());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));

function sendError(res, reason) {
  res.status(500).json({ error: String(reason) });
}

function sendResult(res, result) {
  res.json({ result });
}


passport.serializeUser(function(user, done) {
  done(null, user._id);
});
passport.deserializeUser(function(id, done) {
  db.users.findOne({ _id: mongodb.ObjectId(id) })
      .then(user => {
        if(user) {
          done(null, user);
        } else {
          done("No user", null);
        }
      })
      .catch(err => done(err, null));
});
passport.use(new LocalStrategy((username, password, done) => {
  db.users.findOne({
    login: username,
    passwordHash: hash(password)
  })
      .then(user => {
        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      })
      .catch(err => {
        console.log(err);
        done(err, null);
      });
}));

function hash(pass) {
  return crypto.createHash('md5').update(pass + salt).digest("hex");
}

app.get('/logout', csrfProtection, (req, res) => {
  req.logout();
  res.redirect('/');
});

app.post('/login', csrfProtection,
    passport.authenticate('local', { failureRedirect: '/' }),
    (req, res) => res.redirect('/'));

app.post('/delete_recipe/:recipe_id', csrfProtection, (req, res) => {
    db.recipes.findOne({_id: mongodb.ObjectId(req.params.recipe_id)})
        .then(recipe => {
            let author_login = recipe.author_login;
            db.users.findOne({ login: author_login})
                .then(user => {
                    let num = user.recipesNumber;
                    num--;
                    db.users.findAndModify({
                        query: {login: author_login},
                        update: {$set: {recipesNumber: num}},
                        new: false
                    })
                        .then(() => {
                            db.recipes.remove({
                                _id: mongodb.ObjectId(req.params.recipe_id)
                            }, true)
                                .then(() => {
                                    db.recipes.find()
                                        .then(recipes => res.render("recipes", {recipes: recipes, user: req.user}))
                                })
                                .catch(err => sendError(res, err));
                        })
                })
        })
        .catch(err => sendError(res, err));
});

app.post('/register', csrfProtection,
    (req, res) => {
      let login = req.body.login;
      let firstName = req.body.firstName;
      let lastName = req.body.lastName;
      let pass = req.body.inputPassword;
      let pass2 = req.body.confirmPassword;
      if (!login || !pass || pass !== pass2 || !firstName || firstName == "" ||
              !lastName || lastName == "" || pass.length < 6) {
        sendError(res, "Invalid input!");
      }
      let registrationDate = Date();
      db.users.findOne({ login: login})
          .then(x => {
            if (x) {
              res.sendError(res, "login is busy");
            } else {
                db.default.findOne({ name: "default_avatar"})
                    .then((ava) => {
                        db.avatars.insert({
                            image: ava.image
                        })
                            .then((x) => {
                                db.users.insert({
                                    login,
                                    passwordHash: hash(pass),
                                    firstName,
                                    lastName,
                                    role : "user",
                                    avatar_id : x._id,
                                    recipesNumber: 0,
                                    registrationDate
                                })
                                    .then(() => res.redirect('/'))
                                    .catch(err => res.status(500).end(err));
                            })
                            .catch(err => res.status(500).end(err));
                    })
                    .catch(err => res.status(500).end(err));
            }
          })
          .catch(err => res.status(500).end(err));
    });

app.get('/checklogin', (req, res) => {
    db.users.findOne({login : req.query.login})
        .then(book => {
            if(book)
                res.sendStatus(400);
            else res.sendStatus(200);
        })
        .catch(err => sendError(res, err));
});

app.get('/checkrecipename', (req, res) => {
    db.recipes.findOne({name : req.query.name})
        .then(recipe => {
            if(recipe)
                res.sendStatus(400);
            else res.sendStatus(200);
        })
        .catch(err => sendError(res, err));
});

app.get('/', csrfProtection, (req,res) =>{
    console.log("in main page!");
    db.users.find().sort({
        recipesNumber: -1
    }).limit(4)
        .then(authors => {
            if(authors.length > 3) {
                db.avatars.findOne({_id: mongodb.ObjectId(authors[0].avatar_id)})
                    .then(ava1 => {
                        db.avatars.findOne({_id: mongodb.ObjectId(authors[1].avatar_id)})
                            .then(ava2 => {
                                db.avatars.findOne({_id: mongodb.ObjectId(authors[2].avatar_id)})
                                    .then(ava3 => {
                                        db.avatars.findOne({_id: mongodb.ObjectId(authors[3].avatar_id)})
                                            .then(ava4 => {
                                                res.render("index", {
                                                    user: req.user, authors: authors,
                                                    ava1, ava2, ava3, ava4, csrfToken: req.csrfToken()
                                                });
                                            })
                                    })
                            })
                    })
            }else{
                res.render("index", {
                    user: req.user, authors: null, csrfToken: req.csrfToken()
                });
            }
        })
        .catch(err => sendError(res, err));
});

app.get('/profile/:user_login', csrfProtection, (req,res) =>{
    let user_login = req.params.user_login;
    if(user_login){
        db.users.findOne({ login : user_login})
            .then(owner => {
                if(owner){
                    db.avatars.findOne({ _id :  mongodb.ObjectId(owner.avatar_id)})
                        .then(ava => {
                            db.recipes.find({ author_login: owner.login })
                                .then(recipes => {
                                    if(recipes){
                                        res.render('user_index', {user: req.user, owner: owner, avatar: ava, recipes: recipes, csrfToken: req.csrfToken()});
                                    }
                                })
                        })
                }
                else{
                    sendError(res, "No user");
                }
            })
            .catch(err => sendError(res, err));
    }
});

app.get('/profile', csrfProtection, (req,res) =>{
  if(req.user){
    db.avatars.findOne({ _id :  mongodb.ObjectId(req.user.avatar_id)})
        .then(ava => {
            db.recipes.find({ author_login: req.user.login })
                .then(recipes => {
                    if (recipes) {
                        res.render('user_index', {user: req.user, owner: req.user, avatar: ava, recipes: recipes, csrfToken: req.csrfToken()});
                    }
                })
        })
        .catch(err => sendError(res, err));
  }
  else{
    sendError(res, "Not authorised");
  }
});

app.get('/search', (req, res) => {
    if(req.query.search == "") {
        db.recipes.find()
            .then(recipes => {
                res.send(JSON.stringify(recipes))
            })
            .catch(err => sendError(res, err));
    }
    else {
        db.recipes.find({name: req.query.search})
            .then(recipes => {
                res.send(JSON.stringify(recipes))
            })
            .catch(err => sendError(res, err));
    }
});

app.get('/recipes', csrfProtection, (req, res) => {
  db.recipes.find()
      .then(recipes => res.render("recipes", {recipes: recipes, user: req.user, csrfToken: req.csrfToken()}))
      .catch(err => sendError(res, err));
});

app.get('/recipes/category/:recipe_category', csrfProtection, (req, res) => {
    let category = req.params.recipe_category;
    db.recipes.find({category: category})
        .then(recipes => {
            res.render("recipes", {recipes: recipes, user: req.user, csrfToken: req.csrfToken()});
        })
        .catch(err => sendError(res, err));
});

app.get('/recipes/kitchen/:recipe_kitchen', csrfProtection, (req, res) => {
    let kitchen = req.params.recipe_kitchen;
    db.recipes.find({kitchen: kitchen})
        .then(recipes => {
            res.render("recipes", {recipes: recipes, user: req.user, csrfToken: req.csrfToken()});
        })
        .catch(err => sendError(res, err));
});

app.get('/recipes/:recipe_name', csrfProtection, (req, res) => {
    let name = req.params.recipe_name;
    db.recipes.findOne({name: name})
        .then(recipe => {
            db.users.findOne({ login: recipe.author_login })
                .then(author =>  {
                    db.avatars.findOne({ _id: mongodb.ObjectId(author.avatar_id)})
                        .then(ava => res.render("recipe_index", {recipe: recipe, author: author, avatar: ava, user: req.user, csrfToken: req.csrfToken()}))
                })
        })
        .catch(err => sendError(res, err));
});

app.get("/addrecipe", csrfProtection, (req, res) =>{
    res.render("addrecipe", {user: req.user, csrfToken: req.csrfToken()});
});

//
//   UPLOADING RECIPE
//
app.post('/upload-recipe', csrfProtection, (req, res) => {
    if(!req.body.name || !req.body.author_login){
        sendError(res, "Error, no name or no author")
    }
    else {
        let name = req.body.name;
        db.recipes.findOne({name : name})
            .then(recipe => {
                if(recipe){sendError(res, "Error, name is busy")}
                else{
                    let author_login = req.body.author_login;
                    db.users.findOne({login : author_login})
                        .then((author) => {
                            if(author){
                                let ingredient0 = {
                                    name: req.body.ingredient_name0,
                                    quantity: req.body.ingredient_count0,
                                    measurement: req.body.ingredient_measurement0
                                };
                                let ingredient1 = {
                                    name: req.body.ingredient_name1,
                                    quantity: req.body.ingredient_count1,
                                    measurement: req.body.ingredient_measurement1
                                };
                                let ingredient2 = {
                                    name: req.body.ingredient_name2,
                                    quantity: req.body.ingredient_count2,
                                    measurement: req.body.ingredient_measurement2
                                };
                                let ingredient3 = {
                                    name: req.body.ingredient_name3,
                                    quantity: req.body.ingredient_count3,
                                    measurement: req.body.ingredient_measurement3
                                };
                                let ingredients = { ingredient0, ingredient1, ingredient2, ingredient3 };
                                let instruction = req.body.instruction;
                                let portion_quantity = req.body.portion_count;
                                let cooking_time = {
                                    hours: req.body.cooking_time_hours,
                                    minutes: req.body.cooking_time_minutes
                                };
                                let category = req.body.category;
                                let kitchen = req.body.kitchen;
                                let date = Date();
                                let file = req.files.image;
                                if(!file) res.sendError(res, "Error while convertion image");
                                let image = file.data.toString('base64');
                                db.recipes.insert({
                                    name,
                                    author_login,
                                    ingredients,
                                    instruction,
                                    portion_quantity,
                                    cooking_time,
                                    category,
                                    kitchen,
                                    date,
                                    image
                                })
                                    .then(() => {
                                        db.users.findOne({ login: author_login})
                                            .then(user => {
                                                let num = user.recipesNumber;
                                                num++;
                                                db.users.findAndModify({
                                                    query: {login: author_login},
                                                    update: {$set: {recipesNumber: num}},
                                                    new: false
                                                })
                                                    .then(() => res.redirect('/recipes'))
                                            })
                                    })
                                    .catch(err => res.sendError(res, "Error while adding!"));
                            }
                            else sendError(res, "Error, author doesn`t exist");
                        })
                        .catch(err => sendError(res, err));
                }
            })
            .catch(err => sendError(res, err));
    }
});

app.post('/upload-default-avatar', csrfProtection, (req, res) =>{
    let avaFile = req.files.avatar;
    if(avaFile) {
        let base64String = avaFile.data.toString('base64');
        db.default.remove({name: "default_avatar"})
            .then(() => {
                db.default.insert({
                    name: "default_avatar",
                    image: base64String
                })
                    .then(() => res.redirect('/profile'))
            })
            .catch(err => sendError(res, err));
    }
});

app.post('/upload', csrfProtection, (req, res) =>{
  let avaFile = req.files.avatar;
  if(avaFile) {
    let base64String = avaFile.data.toString('base64');
    db.avatars.insert({
      image: base64String
    })
        .then((x) => {
          db.users.findOne({_id: mongodb.ObjectId(req.user._id)})
              .then((user) => {
                if (user) {
                  db.avatars.remove({_id: user.avatar_id})
                      .then(() => {
                          db.users.findAndModify({
                            query: {_id: req.user._id},
                            update: {$set: {avatar_id: x._id}},
                            new: false
                          })
                              .then(() => res.redirect('/profile'))
                      })
                      .catch(err => sendError(res, err));
                }
              })
        })
        .catch(err => res.status(500).end(err));
  }
  else {
    res.redirect('/profile');
  }
});

var port = normalizePort(process.env.PORT || '3000');

app.listen(port, () => console.log("Course-work listen at 3000"));

function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }
    return false;
}
