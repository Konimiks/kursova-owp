const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const mongodb = require('promised-mongo');

const url = 'mongodb://localhost:27017/course-work';
const db = mongodb(url);

function sendError(res, reason) {
    res.status(500).json({ error: String(reason) });
}

function sendResult(res, result) {
    res.json({ result });
}

router.use(bodyParser.urlencoded({ extended: true}));
router.use(bodyParser.json());

router.delete('/delete_user/:user_id', (req, res) => {
    db.users.remove({
        _id: mongodb.ObjectId(req.params.user_id)
    }, true)
        .then(x => sendResult(res, x))
        .catch(err => sendError(res, err));
});

router.get('/users', (req, res) => {
    db.users.find()
        .then(users => sendResult(res, users))
        .catch(err => sendError(res, err));
});

module.exports = router;